import createNumberSlice from '@app/util/createNumberSlice';

const coinsSlice = createNumberSlice('coins', 0);
export const coinsReducer = coinsSlice.reducer;
export const decrementCoins = coinsSlice.actions.decrement;
export const incrementCoins = coinsSlice.actions.increment;

const greenCoins = createNumberSlice('greenCoins', 0);
export const greenCoinsReducer = greenCoins.reducer;
export const decrementGreenCoins = greenCoins.actions.decrement;
export const incrementGreenCoins = greenCoins.actions.increment;

const blueCoins = createNumberSlice('blueCoins', 0);
export const blueCoinsReducer = createNumberSlice('blueCoins', 0).reducer;
export const decrementBlueCoins = blueCoins.actions.decrement;
export const incrementBlueCoins = blueCoins.actions.increment;
