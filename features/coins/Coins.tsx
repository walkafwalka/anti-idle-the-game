import { Image, Text, View } from 'react-native';
import { useAppSelector } from '@app/hooks';

export const Coins = () => {
  const coins = useAppSelector((state) => state.coins.value);

  return (
    <View style={{ flexDirection: 'row', alignContent: 'center' }}>
      <Image style={{ height: 26, width: 26 }} source={require('@assets/images/9807-coin.png')} />
      <Text style={{ color: '#ffff00', fontFamily: 'PF Tempesta Seven', fontSize: 11, lineHeight: 26, marginLeft: 3 }}>{coins.toLocaleString()}</Text>
    </View >
  );
}

export const BlueCoins = () => {
  const coins = useAppSelector((state) => state.blueCoins.value);

  return (
    <View style={{ flexDirection: 'row', alignContent: 'center' }}>
      <Image style={{ height: 26, width: 26 }} source={require('@assets/images/9807-blueCoin.png')} />
      <Text style={{ color: '#0099ff', fontFamily: 'PF Tempesta Seven', fontSize: 11, lineHeight: 26, marginLeft: 3 }}>{coins.toLocaleString()}</Text>
    </View >
  );
}

export const GreenCoins = () => {
  const coins = useAppSelector((state) => state.greenCoins.value);

  return (
    <View style={{ flexDirection: 'row', alignContent: 'center' }}>
      <Image style={{ height: 26, width: 26 }} source={require('@assets/images/9807-greenCoin.png')} />
      <Text style={{ color: '#00ff00', fontFamily: 'PF Tempesta Seven', fontSize: 11, lineHeight: 26, marginLeft: 3 }}>{coins.toLocaleString()}</Text>
    </View >
  );
}
