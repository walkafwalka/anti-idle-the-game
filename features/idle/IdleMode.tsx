import { Pressable, Text, View } from 'react-native';
import { useAppDispatch, useAppSelector } from '@app/hooks';
import { toggleIdleMode } from '@features/idle/idleSlice';
import { AppDispatch } from '@app/store';

const IdleMode = () => {
  const features = useAppSelector((state) => state.features.value);
  const idleMode = useAppSelector((state) => state.idleMode.value);

  const dispatch: AppDispatch = useAppDispatch();

  if (!features.idleMode) return (
    <Pressable style={{ borderWidth: 2, width: 100 }}>
      <View style={{ backgroundColor: "#0a0807" }}>
        <Text> </Text>
      </View>
      <View style={{ backgroundColor: "#000000" }}>
        <Text style={{ color: "#ffffff", fontFamily: 'PF Tempesta Seven Bold', fontSize: 10, lineHeight: 15, textAlign: 'center' }}>LOCKED</Text>
      </View>
    </Pressable>
  )

  return (
    <Pressable onPress={() => dispatch(toggleIdleMode())} style={{ borderWidth: 2, width: 100 }}>
      <View>
        <View style={{ backgroundColor: idleMode ? "#80d104" : "#4f4c8a" }}>
          <Text style={{ color: idleMode ? '#006600' : "#333366", fontFamily: 'PF Tempesta Seven Bold', fontSize: 10, textAlign: 'center' }}>Idle Mode</Text>
        </View>
        <View style={{ backgroundColor: "#000000" }}>
          <Text style={{ color: idleMode ? "#ffffff" : "#666666", fontFamily: 'PF Tempesta Seven Bold', fontSize: 10, lineHeight: 15, textAlign: 'center' }}>{idleMode ? "ON" : "OFF"}</Text>
        </View>
      </View>
    </Pressable>
  );
}

export default IdleMode;
