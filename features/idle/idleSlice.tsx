import createBooleanSlice from "@app/util/createBooleanSlice";

const idleModeSlice = createBooleanSlice('idleMode', false);
export const idleModeReducer = idleModeSlice.reducer;
export const toggleIdleMode = idleModeSlice.actions.toggle;
