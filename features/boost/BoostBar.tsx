import { Text, View } from 'react-native';

import { useAppSelector } from '@app/hooks';

const BoostBar = () => {
  const boost = useAppSelector((state) => state.boost.value);

  return (
    <View>
      <Text style={{ color: "#ffffff" }}>Boost: {boost}%</Text>
    </View>
  );
}

export default BoostBar;
