import createNumberSlice from '@app/util/createNumberSlice';

const boostSlice = createNumberSlice('boost', 100);
export const boostReducer = boostSlice.reducer;
export const decrementBoost = boostSlice.actions.decrement;
export const incrementBoost = boostSlice.actions.increment;
