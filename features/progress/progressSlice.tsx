import createNumberSlice from '@app/util/createNumberSlice';

const progressSlice = createNumberSlice('progress', 0);
export const progressReducer = progressSlice.reducer;
export const decrementProgress = progressSlice.actions.decrement;
export const incrementProgress = progressSlice.actions.increment;

const progressSpeedManualSlice = createNumberSlice('progressSpeedManual', 100);
export const progressSpeedManualReducer = progressSpeedManualSlice.reducer;
export const decrementProgressSpeedManual = progressSpeedManualSlice.actions.decrement;
export const incrementProgressSpeedManual = progressSpeedManualSlice.actions.increment;

const progressSpeedAutoSlice = createNumberSlice('progressSpeedAuto', 30);
export const progressSpeedAutoReducer = progressSpeedAutoSlice.reducer;
export const decrementProgressSpeedAuto = progressSpeedAutoSlice.actions.decrement;
export const incrementProgressSpeedAuto = progressSpeedAutoSlice.actions.increment;

const progressStore = createNumberSlice('progressStore', 100);
export const progressStoreReducer = progressStore.reducer;
export const decrementProgressStore = progressStore.actions.decrement;
export const incrementProgressStore = progressStore.actions.increment;

const rewardsClaimedSlice = createNumberSlice('rewardsClaimed', 0);
export const rewardsClaimedReducer = rewardsClaimedSlice.reducer;
export const decrementRewardsClaimed = rewardsClaimedSlice.actions.decrement;
export const incrementRewardsClaimed = rewardsClaimedSlice.actions.increment;
