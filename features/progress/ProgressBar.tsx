import { useEffect, useState } from 'react';
import { useIdleTimer } from 'react-idle-timer';
import { Text, View } from 'react-native';

import { useAppDispatch, useAppSelector } from '@app/hooks';
import { AppDispatch } from '@app/store';
import { incrementBlueCoins, incrementCoins, incrementGreenCoins } from '@features/coins/coinsSlice';
import { decrementExperienceCurrent, incrementExperience, incrementExperienceCurrent, incrementLevel } from '@features/experience/experienceSlice';
import { decrementProgress, incrementProgress, incrementRewardsClaimed } from '@features/progress/progressSlice';

const fps = 40;

const ProgessBar = () => {
  const boost = useAppSelector((state) => state.boost.value);
  const experienceCurrent = useAppSelector((state) => state.experienceCurrent.value);
  const idleMode = useAppSelector((state) => state.idleMode.value);
  const level = useAppSelector((state) => state.level.value);
  const progress = useAppSelector((state) => state.progress.value);
  const progressSpeedManual = useAppSelector((state) => state.progressSpeedManual.value);
  const progressSpeedAuto = useAppSelector((state) => state.progressSpeedAuto.value);
  const progressStore = useAppSelector((state) => state.progressStore.value);
  const rewardsClaimed = useAppSelector((state) => state.rewardsClaimed.value);

  const dispatch: AppDispatch = useAppDispatch();

  const [idle, setIdle] = useState(false);
  const [lastInterval, setLastInterval] = useState(new Date().getTime());

  const onIdle = () => setIdle(true);
  const onActive = () => setIdle(false);
  useIdleTimer({ onIdle, onActive, timeout: 1000 });

  const getExperienceRequired = () => (level * level * 10 + 10);

  const checkLevel = () => {
    const experienceRequired = getExperienceRequired();

    if (experienceCurrent >= experienceRequired) {
      dispatch(decrementExperienceCurrent(experienceRequired));
      dispatch(incrementLevel(1));

      console.log("You are now Lv. " + (level + 1) + "! Congratulations!");
    }
  }

  function progressUp() {
    let progSpeed;

    if (idleMode == false) progSpeed = progressSpeedManual;
    else progSpeed = progressSpeedAuto;

    if (rewardsClaimed < 5) progSpeed = progSpeed * 2.5;

    let progPercentToGet = 0;
    if (idleMode == true || progress < progressStore) {
      const loops = Math.floor((new Date().getTime() - lastInterval) / (1000 / fps));
      for (let i = 0; i < loops; i++) {
        progPercentToGet += 0.02 * (boost / 100) * (progSpeed / 100);
        if (rewardsClaimed < 5) progPercentToGet += 20 / fps;
        else if (level < 35) progPercentToGet += 0.125 * (boost / 100) * (progSpeed / 100);
      }
    }

    dispatch(incrementProgress(progPercentToGet));
  }

  function claimReward() {
    const experienceRequired = getExperienceRequired();
    const rewardToClaim = Math.ceil(progress / 200);

    dispatch(decrementProgress(100 * rewardToClaim));
    dispatch(incrementRewardsClaimed(rewardToClaim));

    let expToGet = Math.floor(1.4 * boost * Math.pow(level, 0.6) * rewardToClaim);
    let coinToGet = Math.floor(0.35 * boost * Math.pow(level, 0.6) * (0.8 + Math.random() * 0.4) * rewardToClaim);
    let greenCoinToGet = 100 * rewardToClaim;
    let blueCoinToGet = 0;

    if (level < 35) {
      expToGet = Math.floor(expToGet * 0.5);
      if (expToGet > experienceRequired * 2) {
        expToGet = experienceRequired * 2;
      }
    }

    if (rewardsClaimed < 5) {
      expToGet = 4;
      coinToGet = 1000;
      greenCoinToGet = 200;
      blueCoinToGet = 20;
    }

    if (idleMode == false) {
      greenCoinToGet = greenCoinToGet + 100 * rewardToClaim;

      const bcChance = 0.1;
      if (Math.random() < bcChance) blueCoinToGet = blueCoinToGet + rewardToClaim;
    }

    dispatch(incrementExperienceCurrent(expToGet));
    dispatch(incrementExperience(expToGet));
    dispatch(incrementCoins(coinToGet));
    dispatch(incrementGreenCoins(greenCoinToGet));
    dispatch(incrementBlueCoins(blueCoinToGet));

    console.log("Reward claimed! (+" + expToGet + " EXP | +" + coinToGet + " Coins)");
  }

  useEffect(() => {
    const interval = setInterval(() => {
      checkLevel();
      progressUp();

      if (progress >= 100) {
        if (idle == false || idleMode == true) {
          claimReward();
        }
      }

      setLastInterval(new Date().getTime());
    }, 1000 / fps);

    return () => clearInterval(interval);
  });

  return (
    <View style={{ borderWidth: 2, height: 200, justifyContent: "center", width: 20, alignItems: "center" }}>
      <Text style={{ color: "#ffffff", transform: [{ rotate: "-90deg" }] }}>{Math.floor(progress)}%</Text>
    </View>
  );
}

export default ProgessBar;
