import { Button, Text, View } from 'react-native';

import { useAppDispatch, useAppSelector } from '@app/hooks';
import { AppDispatch } from '@app/store';
import { decrementCoins } from '@features/coins/coinsSlice';
import { enableFeature } from '@features/shop/featuresSlice';

interface ShopItemProps {
  description: string;
  feature: string;
  levelRequired: number;
  coinsCost: number;
}

const ShopItem = ({ description, feature, levelRequired, coinsCost }: ShopItemProps) => {
  const coins = useAppSelector((state) => state.coins.value);
  const level = useAppSelector((state) => state.level.value);
  const features = useAppSelector((state) => state.features.value);

  const dispatch: AppDispatch = useAppDispatch();

  const buyFeature = () => {
    if (!features[feature] && coins >= coinsCost && level >= levelRequired) {
      dispatch(decrementCoins(coinsCost));
      dispatch(enableFeature(feature));

      console.log("[" + description + "] successfully purchased! (-" + coinsCost + " Coins)");
    }
  }

  return (
    <View>
      <Text style={{ color: "#ffffff" }}>
        {description}: {coinsCost} Yellow | Level {levelRequired} Required
        {features[feature] || <Button title='Buy' onPress={buyFeature} disabled={level < levelRequired} />}
      </Text>
    </View>
  )
}

const Shop = () => {
  return (
    <View>
      <Text>Shop</Text>
      <ShopItem description="Idle Mode" feature="idleMode" levelRequired={2} coinsCost={500} />
      <ShopItem description="Boost Generator" feature="boostGenerator" levelRequired={5} coinsCost={1000} />
      <ShopItem description="Garden" feature="garden" levelRequired={10} coinsCost={2000} />
    </View>
  );
}

export default Shop;
