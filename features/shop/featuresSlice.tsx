import { createSlice } from '@reduxjs/toolkit';

const initialValue: { [key: string]: boolean } = {
  idleMode: false,
}

const featuresSlice = createSlice({
  name: 'features',
  initialState: {
    value: initialValue
  },
  reducers: {
    disable: (state, action) => { state.value = { ...state.value, [action.payload]: false } },
    enable: (state, action) => { state.value = { ...state.value, [action.payload]: true } },
  }
});

export default featuresSlice;

export const featuresReducer = featuresSlice.reducer;
export const disableFeature = (feature: string) => featuresSlice.actions.disable(feature);
export const enableFeature = (feature: string) => featuresSlice.actions.enable(feature);
