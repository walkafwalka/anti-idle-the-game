import createNumberSlice from '@app/util/createNumberSlice';

const experienceSlice = createNumberSlice('experience', 0);
export const experienceReducer = experienceSlice.reducer;
export const decrementExperience = experienceSlice.actions.decrement;
export const incrementExperience = experienceSlice.actions.increment;

const experienceCurrentSlice = createNumberSlice('experienceCurrent', 0);
export const experienceCurrentReducer = experienceCurrentSlice.reducer;
export const decrementExperienceCurrent = experienceCurrentSlice.actions.decrement;
export const incrementExperienceCurrent = experienceCurrentSlice.actions.increment;

const levelSlice = createNumberSlice('level', 1);
export const levelReducer = levelSlice.reducer;
export const decrementLevel = levelSlice.actions.decrement;
export const incrementLevel = levelSlice.actions.increment;
