import { Text, View } from 'react-native';

import { useAppSelector } from '@app/hooks';

const ExperienceBar = () => {
  const experience = useAppSelector((state) => state.experience.value);
  const level = useAppSelector((state) => state.level.value);

  return (
    <View>
      <Text style={{ color: "#ffffff" }}>EXP: {experience.toLocaleString()} Level: {level.toLocaleString()}</Text>
    </View>
  );
}

export default ExperienceBar;
