import { StatusBar } from 'expo-status-bar';
import { useFonts } from 'expo-font';
import { McLaren_400Regular } from '@expo-google-fonts/mclaren';
import { ComicNeue_400Regular, ComicNeue_700Bold } from '@expo-google-fonts/comic-neue';
import { Image, StyleSheet, Text, View } from 'react-native';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from './app/store';

import ProgessBar from './features/progress/ProgressBar';
import IdleMode from './features/idle/IdleMode';
import AntiIdleTheGameLogo from './app/ui/AntiIdleTheGame';
import { Coins, GreenCoins, BlueCoins } from '@features/coins/Coins';
import ExperienceBar from '@features/experience/ExperienceBar';
import BoostBar from '@features/boost/BoostBar';
import Shop from '@features/shop/Shop';

export default function App() {
  const [fontsLoaded] = useFonts({
    ComicNeue_400Regular, ComicNeue_700Bold,
    McLaren_400Regular,
    'PF Tempesta Seven': require('./assets/fonts/893_PF Tempesta Seven.ttf'),
    'PF Tempesta Seven Bold': require('./assets/fonts/131_PF Tempesta Seven.ttf'),
  });

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
        <View style={styles.outerContainer}>
          <View style={styles.container}>

            <View style={styles.header}>

              <View style={{ flex: 0.35 }}>
                <View style={{ margin: 2 }}>
                  <Coins />
                </View>
                <View style={{ margin: 2, flexDirection: "row" }}>
                  <View style={{ flex: 0.5 }}>
                    <GreenCoins />
                  </View>
                  <View style={{ flex: 0.5 }}>
                    <BlueCoins />
                  </View>
                </View>
              </View>

              <View style={{ alignItems: "center", flex: 0.3, marginTop: 8 }}>
                <Text style={{ color: "#ffffff", textAlign: 'center' }}>
                  <Text style={{ fontFamily: 'ComicNeue_700Bold', lineHeight: 26, fontSize: 18 }}>????</Text>
                  <br />
                  <Text style={{ fontFamily: 'ComicNeue_400Regular', fontSize: 14 }}>Welcome to Anti-Idle!</Text>
                </Text>
              </View>

              <View style={{ flex: 0.35, alignItems: "flex-end" }}>
                <ExperienceBar />
              </View>
            </View>

            <View style={styles.innerContainer}>
              <View style={styles.main}>
                <View>
                  <Image style={{ width: 500, height: 200 }} source={require('./assets/images/7363-dragon1.jpg')} />
                  <Image style={{ width: 500, height: 150 }} source={require('./assets/images/7364-dragon2.png')} />
                </View>
                <View></View>
              </View>
              <View style={styles.sidebar}>

                <View style={{ alignItems: 'center', marginBottom: 15 }}>
                  <AntiIdleTheGameLogo />
                </View>

                <View style={{ alignItems: 'center', marginBottom: 10 }}>
                  <ProgessBar />
                  <IdleMode />
                </View>

                <View>
                  <BoostBar />
                </View>

              </View>
            </View>

            <View>
              <Shop />
            </View>

          </View>
          <StatusBar style="auto" />
        </View >
      </PersistGate>
    </Provider>
  );
}

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    backgroundColor: '#000000',
    color: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    width: 650,
  },
  header: {
    backgroundColor: "#0f0c0a",
    color: "#ffffff",
    flexDirection: "row",
    padding: 5,
  },
  innerContainer: {
    backgroundColor: "#1e1914",
    flexDirection: "row"
  },
  main: {
    margin: 10,
    width: 500,
  },
  sidebar: {
    margin: 10,
    marginLeft: 0,
    width: 120,
  },
});
