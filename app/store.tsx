import { combineReducers, configureStore } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer, persistStore } from 'redux-persist';

import { boostReducer } from '@features/boost/boostSlice';
import { blueCoinsReducer, coinsReducer, greenCoinsReducer } from '@features/coins/coinsSlice';
import { experienceCurrentReducer, experienceReducer, levelReducer } from '@features/experience/experienceSlice';
import { idleModeReducer } from '@features/idle/idleSlice';
import { progressReducer, progressSpeedAutoReducer, progressSpeedManualReducer, progressStoreReducer, rewardsClaimedReducer } from '@features/progress/progressSlice';
import { featuresReducer } from '@features/shop/featuresSlice';

const rootReducer = combineReducers({
  coins: coinsReducer,
  greenCoins: greenCoinsReducer,
  blueCoins: blueCoinsReducer,

  experience: experienceReducer,
  experienceCurrent: experienceCurrentReducer,
  level: levelReducer,

  idleMode: idleModeReducer,

  progress: progressReducer,
  progressSpeedManual: progressSpeedManualReducer,
  progressSpeedAuto: progressSpeedAutoReducer,
  progressStore: progressStoreReducer,
  rewardsClaimed: rewardsClaimedReducer,

  boost: boostReducer,
  features: featuresReducer,
})

const persistedReducer = persistReducer({
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['progress'],
}, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const persistor = persistStore(store);
