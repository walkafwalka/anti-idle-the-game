import { createSlice } from '@reduxjs/toolkit';

const createNumberSlice = (name: string, value: number) => {
  return createSlice({
    name,
    initialState: { value },
    reducers: {
      decrement: (state, action) => { state.value -= action.payload },
      increment: (state, action) => { state.value += action.payload },
    }
  });
}

export default createNumberSlice;
