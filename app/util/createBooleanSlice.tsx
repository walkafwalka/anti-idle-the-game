import { createSlice } from '@reduxjs/toolkit';

const createBooleanSlice = (name: string, value: boolean) => {
  return createSlice({
    name,
    initialState: { value },
    reducers: {
      toggle: (state) => { state.value = !state.value },
    }
  });
}

export default createBooleanSlice;
