import Svg, { Text } from 'react-native-svg';

const AntiIdleTheGameLogo = () => {
  return (
    <Svg height="50" width="100" fontFamily='McLaren_400Regular' fontWeight='bold' stroke='#000000' strokeOpacity='0.8'>
      <Text fill="#ff0000" fontSize="26" y="22" x="0">A</Text>
      <Text fill="#ff0000" fontSize="20" y="22" x="20">nti</Text>
      <Text fill="#ffff00" fontSize="23" y="24" x="48">-</Text>
      <Text fill="#0066ff" fontSize="20" y="22" x="61">Idle</Text>
      <Text fill="#00ff00" fontSize="15" y="46" x="10">The Game</Text>
    </Svg>
  );
}

export default AntiIdleTheGameLogo;
